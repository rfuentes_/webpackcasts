import small from '../assets/small.jpg';
import '../styles/image_viewer.css';

export default () => {
  const image = document.createElement('img');
  image.src = small;

  document.body.appendChild(image);


  const myObject = {
    one: 1,
    two: 2,
    three: 3
  };

  console.log('one', Object.assign({}, ...myObject));

  console.log('two', myObject);
  console.log('three', [...myObject]);
  console.log('four', ...myObject);


  const coffee = {
    ...myObject,
    hola: 'amigos'
  };

  console.log(coffee);

};
